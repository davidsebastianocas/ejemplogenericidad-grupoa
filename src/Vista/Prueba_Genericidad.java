/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Persona;
import Util.ManejoVector_Generico;

/**
 *
 * @author madarme
 */
public class Prueba_Genericidad {
 
    public static void main(String[] args) {
         String nombres[]={"Brayan", "Carlos","David","Gederson"};
        Integer codigos[]={1152030,1152031,1152032};
        Float notas[]={3.4F,4F,5F,3.2F};
        Float notas2[]={3.4F,4F,5F,3.2F};
        Persona personas[]={new Persona(1,"Brayan",3,4,2000),new Persona(2,"Gederson",4,14,1999)};
        Persona personas2[]={new Persona(1,"Brayan",3,4,2000),new Persona(2,"Gederson",4,14,1999)};
        
        //Usando nuestra clase génerica: <PARAMETRIZACIÓN>
        ManejoVector_Generico<String> v1=new ManejoVector_Generico(nombres);
        ManejoVector_Generico<Integer> v2=new ManejoVector_Generico(codigos);
        ManejoVector_Generico<Float> v3=new ManejoVector_Generico(notas);
        
        ManejoVector_Generico<Persona> v4=new ManejoVector_Generico(personas);
        ManejoVector_Generico<Persona> v5=new ManejoVector_Generico(personas2);
        
        ManejoVector_Generico<Float> v6=new ManejoVector_Generico(notas2);
        
        
        System.out.println(v1.toString());
        System.out.println(v2.toString());
        System.out.println(v3.toString());
        System.out.println(v4.toString());
        
        //Probando equals:
        
        System.out.println("notas vs notas2: "+v3.equals(v6));
        System.out.println("persona vs personas2: "+v5.equals(v4));
        
        //Probando ordenamiento: 
        
        v1.ordenarInsercion();
        v2.ordenarInsercion();
        v3.ordenarInsercion();
        v4.ordenarInsercion();
        
        //Vectores ordenados:
        System.out.println("Vectores ordenados:");
        System.out.println(v1.toString());
        System.out.println(v2.toString());
        System.out.println(v3.toString());
        System.out.println(v4.toString());
        
    }
}
